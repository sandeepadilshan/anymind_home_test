import { Dimensions } from 'react-native'

const common = {
  white: '#fff',
  transparent: 'transparent',
  black: '#000',
  blue: 'blue',
}

const scale = value => {
  const { width } = Dimensions.get('window').width
  const baseWidth = 320
  const scaledValue = (width/baseWidth) * value
  return scaledValue
}

const Themes = {
    COLORS: {
        ...common,
        primary: '#007BC3',
        wildBlue: '#808CBA',
        textPrimary: '#333333',
    },
    scale
}

export default Themes