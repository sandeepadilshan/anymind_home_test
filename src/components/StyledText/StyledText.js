import React from 'react'
import { Text } from 'react-native'
import { ScaledSheet } from 'react-native-size-matters'
import Themes from './../../assets/themes'
import { StyledTitle, ChannelTitle } from './styles'

const styles = ScaledSheet.create({
  txtDefault: {
    fontSize: '14@ms',
    color: Themes.COLORS.textPrimary,
  },
})
const StyledText = ({
  titleText,
  fontSize,
  color,
  numberOfLines,
}) => {
  return (
    <StyledTitle>
      <ChannelTitle
        fontSize={fontSize}
        numberOfLines={numberOfLines}
        channelTitleColor={color}
      >
        {titleText}
      </ChannelTitle>
    </StyledTitle>
  )
}

export default StyledText