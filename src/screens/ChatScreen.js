import React, {useState} from 'react'
import { Button, View, Text, TextInput } from 'react-native'
import { ScaledSheet } from 'react-native-size-matters'
import FetchChats from '../modules/FetchChats'
import PostMessage from '../modules/PostMessage'

const ChatScreen = ({ navigation, route }) => {
  const [user, setUser] = useState(route.params.userId)
  const [channelData, setChannelData] = useState(route.params.channelData)

  return (
    <View style={styles.chatContainer}>
      <FetchChats/>
      <PostMessage/>
    </View>
  );
}

export default ChatScreen

const styles = ScaledSheet.create({
  chatContainer: {
    flex: 1, 
    justifyContent: 'flex-end', 
    alignItems: 'flex-end', 
    marginRight: 16,
    marginBottom:40
  }
})