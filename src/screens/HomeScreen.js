import React, { useState} from 'react'
import { ActivityIndicator, View, Text } from 'react-native'
import { ScaledSheet } from 'react-native-size-matters'
import { FlatList, TouchableOpacity } from 'react-native-gesture-handler'
import Themes from '../assets/themes'

import StyledText from '../components/StyledText/StyledText'

const HomeScreen = ({ navigation }) => {
  const [isLoading, setIsLoading] = useState(false)
  const [channels, setChannels] = useState(
    [
      {"id": "1", "channelId": "LGTM"}, 
      {"id": "2", "channelId": "General"},
      {"id": "3", "channelId": "Technology"}
    ])
  const [user, setUser] = useState(
    [
      {"userId": "Sam"}, 
      {"userId": "Russell"},
      {"userId": "Joyse"}
    ])
  
  const goToChatScreen = (item) => {
    navigation.navigate('ChatScreen',
      { channelData: item?.item, 
        userId: user?.[0]?.userId  
      })
  }  
  const renderChanel = (item) => {
    return(
      <View>
        <TouchableOpacity onPress={() => goToChatScreen(item) }>
          <View style={styles.containerChannelRow}>
            <StyledText 
              titleText={item?.item?.channelId} 
              color={Themes.COLORS.primary} 
              fontSize={'18'}
            />
          </View>
        </TouchableOpacity>
      </View>
    )
  }

  const renderChanels = () =>{
    if(isLoading){
      return(
        <ActivityIndicator size={'small'} color={Themes.COLORS.blue} />
      )
    }
    if(channels.length > 0){
      return(
        <View style={{flex:1}}>
          <FlatList
            data={channels}
            renderItem={renderChanel}
            keyExtractor={item => item.id}
            horizontal={false}
            showsHorizontalScrollIndicator={false}
            showsVerticalScrollIndicator={false}
          />
        </View>
      )
    }
  }

  return (
    <View style={styles.containerChannel}>
      <StyledText 
        titleText={'Choose your Channel'} 
        color={Themes.COLORS.textPrimary} 
        fontSize={'18'}
      />
      {renderChanels()}
    </View>
  );
}

export default HomeScreen

const styles = ScaledSheet.create({
  containerChannel:{
    flex: 1, 
    justifyContent: 'flex-start', 
    alignItems: 'flex-start', 
    margin:'16@ms'
  },
  channelTitle: {
    fontSize: '16@ms',
    color: Themes.COLORS.primary,
    fontWeight:'bold',
    marginVertical: '8@ms'
  },
  containerChannelRow: {
    flexDirection: 'row', 
    justifyContent:'flex-start'  
  }
})