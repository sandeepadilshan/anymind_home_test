import React, {useState} from 'react'
import {
    useMutation,
    gql,
  } from "@apollo/client";
import { Button, View, Text, TextInput } from 'react-native'

const SEND_MESSAGE = gql`
mutation postMessage($channelId: String!, $text: String!, $userId: String!) {
  postMessage(data: { channelId: $channelId, text: $text, userId: $userId }) {
    messageId
  }
}
`
const postMessage = () => {
    const [text, setText] = useState('')
    const [postMessage, { data, loading, error }] = useMutation(SEND_MESSAGE);
  
    const handleText = (event) => {
        console.info(event)
        setText(event)
    }
    
    if (loading) return(<Text>{'Submitting...'}</Text>) ;
    if (error) return(<Text>{ `Submission error! ${error.message}`}</Text>);
  
    return (
        <View style={{flexDirection: 'row'}}>
        <TextInput
            onChangeText={ handleText}
            value={text}
            placeholder="Type Your Message Here"
        />
        <Button
            onPress={() => {
                postMessage({
                    variables: {
                        channelId: "1",
                        text: "text",
                        userId: "Sam"
                      }
                    })
                      .then(res => res)
                      .catch(err => <Text>{err}</Text>);
            } }
            title="Send"
        />
      </View>
    );
  }

export default postMessage  