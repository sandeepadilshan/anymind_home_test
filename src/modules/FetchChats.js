import React from 'react'
import {
    useQuery,
    gql,
  } from "@apollo/client";
import { Button, View, Text, TextInput } from 'react-native'

  const MESSAGES_FETCH_LATEST = gql`
  {
    fetchLatestMessages(channelId: "1"){
      text
      messageId
      datetime
      userId
    }
  }`;

  const FetchChats = () =>{
    const { loading, error, data } = useQuery(MESSAGES_FETCH_LATEST);
    if (loading) return <Text>Loading...</Text>;
    if (error) return <Text>Error :(</Text>;
  
    return data?.fetchLatestMessages?.map(({ text, userId }) => (
      <View key={messageId}>
        <Text>
          {messageId}: {text} : {userId}
        </Text>
      </View>
    ));
  }

  export default FetchChats
