import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import {
  ApolloClient,
  InMemoryCache,
  ApolloProvider,
  gql,
} from "@apollo/client";
import AppStack from './navigation/AppStack'

const client = new ApolloClient({
  uri: 'https://angular-test-backend-yc4c5cvnnq-an.a.run.app/graphiql',
  cache: new InMemoryCache()
});

client
  .query({
    query: gql`
    {
      fetchLatestMessages(channelId: "1"){
        text
        messageId
        datetime
        userId
      }
    }
    `
  })
  .then(result => console.info(result));

const App = () => {
  return (
    <ApolloProvider client={client}>
      <NavigationContainer>
        <AppStack />
      </NavigationContainer>
    </ApolloProvider>
  );
}

export default App
