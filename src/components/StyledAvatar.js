import { View, Text, Image } from 'react-native'
import React from 'react'
import { scale, ScaledSheet } from 'react-native-size-matters'
import Themes from '../assets/themes'

export default function StyledAvatar({
  height = 65,
  width = 65,
  avatarUrl,
  avatarFaceText = '',
  sizeText = 16,
}) {
  if (avatarUrl.uri) {
    return (
      <Image
        source={avatarUrl}
        style={[
          styles.avatar,
          {
            width: scale(width),
            height: scale(height),
            borderRadius: scale(width / 2),
          },
        ]}
      />
    )
  }
  return (
    <View
      style={
       {
        backgroundColor: Themes.COLORS.primary,
       }
      }
    >
      <Text style={[styles.avatarFaceText, { fontSize: sizeText }]}>
        {avatarFaceText}
      </Text>
    </View>
  )
}

const styles = ScaledSheet.create({
  avatarFaceView: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  avatarFaceText: {
    fontWeight: 'bold',
    color: Themes.COLORS.white,
  },
})