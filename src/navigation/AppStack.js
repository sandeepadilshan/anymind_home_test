import * as React from 'react';
import { View, Button, Text } from 'react-native';
import {
  createStackNavigator,
  HeaderStyleInterpolators,
} from '@react-navigation/stack';
import Themes from '../assets/themes';

import HomeScreen from './../screens/HomeScreen'
import ChatScreen from './../screens/ChatScreen'

const Stack = createStackNavigator();

const AppStack = () => {
  return (
    
      <Stack.Navigator>
        <Stack.Screen
            name="Home"
            component={HomeScreen}
            options={{
            title: 'AnyChat',
            headerTintColor: 'white',
            headerStyle: { backgroundColor: Themes.COLORS.primary },
            }}
        />
        <Stack.Screen
            name="ChatScreen"
            component={ChatScreen}
            options={{
            title: 'ChatScreen',
            headerStyleInterpolator: HeaderStyleInterpolators.forUIKit,
            }}
        />
      </Stack.Navigator>
  );
}

export default AppStack
