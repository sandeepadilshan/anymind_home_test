import styled from '@emotion/native'
import Themes from '../../assets/themes'

export const StyledTitle = styled.View`
  flex-shrink: 1px;
  height: 44px;
  align-items: center;
  padding: 0px 8px;
  border-radius: 5px;
  flex-direction: row;
`

export const ChannelTitle = styled.Text`
  font-size: ${p => p.fontSize || 16 }px;
  color: ${p => p.channelTitleColor || Themes.COLORS.white};
`